﻿using System.Collections.Generic;

namespace Property_Project.Models
{
    // Class for the current property status
    public class PropertyStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Property> Properties { get; set; }
    }
}
