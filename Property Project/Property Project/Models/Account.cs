﻿using System;
using Microsoft.AspNetCore.Identity;

// Account class that inherits from indentity
namespace Property_Project.Models
{
    public class Account : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime CreatedAt { get; set; }
        public AccountType AccountType { get; set; }
    }
}
