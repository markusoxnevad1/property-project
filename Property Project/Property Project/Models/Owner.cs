﻿using System;
using System.Collections.Generic;

namespace Property_Project.Models
{
    // Class for current and previous owners of properties
    public class Owner
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Phone { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public int SocialSecurityNumber { get; set; }
        public DateTime CreatedAt { get; set; }
        public OwnerType OwnerType { get; set; }
        public ICollection<OwnershipLog> OwnershipLogs { get; set; }
    }
}