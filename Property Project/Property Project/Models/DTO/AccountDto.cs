﻿// DTO for account for handling login 
namespace Property_Project.Models.DTO
{
    public class AccountDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
