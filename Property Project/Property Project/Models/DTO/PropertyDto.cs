﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Property_Project.Models.DTO
{
    // Dto for the catalogue page
    public class PropertyCardInfo
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ShortAddress { get; set; }
        public object PropertyImages { get; set; }
        public string City { get; set; }
    }

    // Contains the basic information a user should see in the property details page
    public class GuestPropertyDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ShortAddress { get; set; }
        public string LongAddress { get; set; }
        public object PropertyImages { get; set; }
        public string PropertyType { get; set; }

    }

    // Extend the basic information with the required information for a buyer
    public class BuyerPropertyDto : GuestPropertyDto
    {
        public string City { get; set; }
        public int Zip { get; set; }
        public DateTime BuiltAt { get; set; }
        public DateTime LastRenovation { get; set; }
    }

    // Extend the buyer information with information required for an agent
    public class AgentPropertyDto : BuyerPropertyDto
    {
        public object OwnershipLogs { get; set; }
        public string CurrentOwnerName { get; set; }
        public int CurrentOwnerTlf { get; set; }
        public string CurrentOwnerEmail { get; set; }
        public int CurrentPropertyValue { get; set; }
        public object Renovations { get; set; }
    }
}
