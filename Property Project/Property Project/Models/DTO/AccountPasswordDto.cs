﻿// DTO for handling password change
namespace Property_Project.Models.DTO
{
    public class AccountPasswordDto
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
