﻿// DTO for when handling account settings
namespace Property_Project.Models.DTO
{
    public class AccountInfoDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int Phone { get; set; }
    }
}
