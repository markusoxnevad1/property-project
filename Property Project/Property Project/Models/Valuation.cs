﻿using System;

namespace Property_Project.Models
{
    // Class for the valuation history of a property
    public class Valuation
    {
        public int Id { get; set; }
        public string Comments { get; set; }
        public int Value { get; set; }
        public DateTime ValuationDate { get; set; }
        public Property Property { get; set; }
    }
}
