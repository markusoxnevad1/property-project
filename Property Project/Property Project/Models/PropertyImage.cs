﻿namespace Property_Project.Models
{
    // Class for property images
    public class PropertyImage
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public Property Property { get; set; }
    }
}
