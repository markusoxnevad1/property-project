﻿using System.Collections.Generic;

namespace Property_Project.Models
{
    // Class for account types
    public class AccountType
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Account> Accounts { get; set; }
    }
}
