﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Property_Project.Models.Seed
{
    public class SeedData
    {
        public List<AccountType> AccountTypesList => new List<AccountType>()
            {
                new AccountType {Id = 1, Name = "AGENT" },
                new AccountType {Id = 2, Name = "BUYER" }
            };

        public List<OwnerType> OwnerTypesList => new List<OwnerType>()
            {
                new OwnerType {Id = 1, Name = "COMPANY" },
                new OwnerType {Id = 2, Name = "PRIVATE" }
            };

        public List<PropertyType> PropertyTypesList => new List<PropertyType>()
            {
                new PropertyType {Id = 1, Name = "COMMERCIAL" },
                new PropertyType {Id = 2,  Name = "APARTMENT" },
                new PropertyType {Id = 3,  Name = "SEMI_DETACHED" },
                new PropertyType {Id = 4,  Name = "HOUSE" }
            };

        public List<PropertyStatus> PropertyStatusList => new List<PropertyStatus>()
            {
                new PropertyStatus {Id = 1, Name = "ON_MARKET" },
                new PropertyStatus {Id = 2,  Name = "AVAILABLE" },
                new PropertyStatus {Id = 3,  Name = "UNDER_RENOVATION" },
                new PropertyStatus {Id = 4,  Name = "UNAVAILABLE" },
                new PropertyStatus {Id = 5,  Name = "HISTORICAL" }
            };

        public List<object> PropertiesList => new List<object>()
            {
                new
                    {
                        Title = "FIGGJO - Large, copious house for sale | 2 kitchens | 3 bathrooms | 3 livingrooms | 7 bedrooms",
                        Value = 3250000,
                        ShortAddress = "Gryvjene 22, Figgjo",
                        LongAddress = "Gryvjene 22, 4332 Figgjo",
                        City = "Sandnes",
                        Country = "Norway",
                        Zip = 4332,
                        CreatedAt = DateTime.UtcNow,
                        PropertyStatusId = 2,
                        PropertyTypeId = 4,
                        BuiltAt = DateTime.ParseExact("11/08/2019", "d/MM/yyyy", CultureInfo.InvariantCulture),
                        Id = 1
                    },
                new
                    {
                        Title = "Rossåstoppen, Figgjo - 4 detached houses for sale | 2 livingrooms | 4 bedrooms | 2 bathrooms",
                        Value = 4390000,
                        ShortAddress = "Granliveien, Figgjo",
                        LongAddress = "Granliveien, 4332 Figgjo",
                        City = "Sandnes",
                        Country = "Norway",
                        Zip = 4332,
                        CreatedAt = DateTime.UtcNow,
                        PropertyStatusId = 2,
                        PropertyTypeId = 4,
                        BuiltAt = DateTime.ParseExact("01/02/2020", "d/MM/yyyy", CultureInfo.InvariantCulture),
                        Id = 2
                    },
                new
                    {
                        Title = "NEW PRICE | Nice apartment on the top of Skadberg close to the previous Norwegian chess champion! Parking in a closed garagefacility",
                        Value = 1295000,
                        ShortAddress = "Kjerrberg terrasse 7, Sola",
                        LongAddress = "Kjerrberg terrasse 7, 4051 Sola",
                        City = "Sola",
                        Country = "Norway",
                        Zip = 4051,
                        CreatedAt = DateTime.UtcNow,
                        PropertyStatusId = 4,
                        PropertyTypeId = 3,
                        BuiltAt = DateTime.ParseExact("03/07/2011", "d/MM/yyyy", CultureInfo.InvariantCulture),
                        Id = 3
                    },
                new
                    {
                        Title = "Skansen - Nyoppført enebolig med lekre kvaliteter og meget attraktiv beliggenhet.",
                        Value = 4600000,
                        ShortAddress = "Telthussmauet 6B, Bergen",
                        LongAddress = "Telthussmauet 6B, 5003 Bergen",
                        City = "Bergen",
                        Country = "Norway",
                        Zip = 5003,
                        CreatedAt = DateTime.UtcNow,
                        PropertyStatusId = 1,
                        PropertyTypeId = 2,
                        BuiltAt = DateTime.ParseExact("11/08/2019", "d/MM/yyyy", CultureInfo.InvariantCulture),
                        Id = 4
                    },
                new
                    {
                        Title = "Sandviken - Pen 3-roms leilighet med stor og solrik balkong. Meget attraktiv beliggenhet. IN-ordning.",
                        Value = 2700000,
                        ShortAddress = "Amalie Skrams vei 16, Bergen",
                        LongAddress = "Amalie Skrams vei 16, 5036 Bergen",
                        City = "Bergen",
                        Country = "Norway",
                        Zip = 5036,
                        CreatedAt = DateTime.UtcNow,
                        PropertyStatusId = 3,
                        PropertyTypeId = 4,
                        BuiltAt = DateTime.ParseExact("11/08/2019", "d/MM/yyyy", CultureInfo.InvariantCulture),
                        Id = 5
                    },
            };

        public List<object> OwnersList => new List<object>()
            {
                new
            {
                Id = 1,
                FirstName = "Bob",
                LastName = "Ross",
                Phone = 92514212,
                Email = "bob.ross@yahoo.com",
                Birthday = DateTime.ParseExact("06/04/1987", "d/MM/yyyy", CultureInfo.InvariantCulture),
                SocialSecurityNumber = 231312,
                OwnerTypeId = 2,
                CreatedAt = DateTime.UtcNow
            },

                new
            {
                Id = 2,
                FirstName = "Steven",
                LastName = "Segal",
                Phone = 98323531,
                Email = "steven.segal@hotmail.com",
                Birthday = DateTime.ParseExact("06/12/1967", "d/MM/yyyy", CultureInfo.InvariantCulture),
                SocialSecurityNumber = 843123,
                OwnerTypeId = 1,
                CreatedAt = DateTime.UtcNow
            }
            };

        public List<object> OwnershipLogsList => new List<object>()
            {
                new
                {
                    DateAcquired = DateTime.ParseExact("03/07/2011", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    DateSold = DateTime.ParseExact("11/08/2016", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    CreatedAt = DateTime.UtcNow,
                    PropertyId = 3,
                    OwnerId = 2,
                    Id = 1
                },
                new
                {
                    DateAcquired = DateTime.ParseExact("11/08/2016", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    DateSold = (DateTime?)null,
                    CreatedAt = DateTime.UtcNow,
                    PropertyId = 3,
                    OwnerId = 1,
                    Id = 2
                },
                new 
                {
                    DateAcquired = DateTime.ParseExact("11/08/2019", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    DateSold = (DateTime?)null,
                    CreatedAt = DateTime.UtcNow,
                    PropertyId = 1,
                    OwnerId = 2,
                    Id = 3
                },
                new
                {
                    DateAcquired = DateTime.ParseExact("01/02/2020", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    DateSold = (DateTime?)null,
                    CreatedAt = DateTime.UtcNow,
                    PropertyId = 2,
                    OwnerId = 2,
                    Id = 4
                },
                new
                {
                    DateAcquired = DateTime.ParseExact("03/07/2011", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    DateSold = DateTime.ParseExact("11/08/2016", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    CreatedAt = DateTime.UtcNow,
                    PropertyId = 4,
                    OwnerId = 2,
                    Id = 5
                },
                new
                {
                    DateAcquired = DateTime.ParseExact("11/08/2016", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    DateSold = (DateTime?)null,
                    CreatedAt = DateTime.UtcNow,
                    PropertyId = 4,
                    OwnerId = 1,
                    Id = 6
                },
                new
                {
                    DateAcquired = DateTime.ParseExact("03/07/2011", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    DateSold = DateTime.ParseExact("11/08/2016", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    CreatedAt = DateTime.UtcNow,
                    PropertyId = 5,
                    OwnerId = 2,
                    Id = 7
                },
                new
                {
                    DateAcquired = DateTime.ParseExact("11/08/2016", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    DateSold = (DateTime?)null,
                    CreatedAt = DateTime.UtcNow,
                    PropertyId = 5,
                    OwnerId = 1,
                    Id = 8
                },
            };

        public List<object> RenovationsList => new List<object>()
            {
                new
                {
                    Description = "Built new bedroom",
                    DateFrom = DateTime.ParseExact("04/05/2012", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    DateTo = DateTime.ParseExact("07/06/2012", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    PropertyId = 3,
                    Id = 1
                },
                new
                {
                    Description = "Built a new parking facility",
                    DateFrom = DateTime.ParseExact("10/09/2015", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    DateTo = DateTime.ParseExact("20/12/2015", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    PropertyId = 3,
                    Id = 2
                },
                new
                {
                    Description = "Extended north side of the house, in order to build a new livingroom, also added an extra bathroom",
                    DateFrom = DateTime.ParseExact("14/02/2019", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    DateTo = DateTime.ParseExact("23/07/2019", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    PropertyId = 1,
                    Id = 3
                },
                new
                {
                    Description = "Built two new bedrooms",
                    DateFrom = DateTime.ParseExact("22/05/2019", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    DateTo = DateTime.ParseExact("06/07/2019", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    PropertyId = 1,
                    Id = 4
                },
                new
                {
                    Description = "Added 2 new bedrooms, and built a new bathroom",
                    DateFrom = DateTime.ParseExact("12/10/2019", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    DateTo = DateTime.ParseExact("18/12/2019", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    PropertyId = 2,
                    Id = 5
                },
                new
                {
                    Description = "Built two new bedrooms",
                    DateFrom = DateTime.ParseExact("22/05/2019", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    DateTo = DateTime.ParseExact("06/07/2019", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    PropertyId = 4,
                    Id = 6
                },
                new
                {
                    Description = "Built two new bedrooms",
                    DateFrom = DateTime.ParseExact("22/05/2019", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    DateTo = DateTime.ParseExact("06/07/2019", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    PropertyId = 5,
                    Id = 7
                },
                new
                {
                    Description = "Extended north side of the house, in order to build a new livingroom, also added an extra bathroom",
                    DateFrom = DateTime.ParseExact("14/02/2019", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    DateTo = DateTime.ParseExact("23/07/2019", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    PropertyId = 5,
                    Id = 8
                },
            };

        public List<object> ValuationsList => new List<object>()
            {
                new 
                {
                    Comments = "1 kitchen, 1 bedroom, 1 bathroom",
                    Value = 700000,
                    ValuationDate = DateTime.ParseExact("03/06/2011", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    PropertyId = 3,
                    Id = 1
                },
                new 
                {
                    Comments = "1 kitchen, 2 bedroom, 1 bathroom, 1 parking",
                    Value = 1295000,
                    ValuationDate = DateTime.ParseExact("11/07/2016", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    PropertyId = 3,
                    Id = 2
                },
                new
                {
                    Comments = "1 kitchen, 2 livingrooms, 4 bedrooms, 2 bathroom",
                    Value = 3250000,
                    ValuationDate = DateTime.ParseExact("11/07/2019", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    PropertyId = 1,
                    Id = 3
                },
                new
                {
                    Comments = "2 kitchens, 3 bathrooms, 3 livingrooms, 7 bedrooms",
                    Value = 4390000,
                    ValuationDate = DateTime.ParseExact("01/01/2020", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    PropertyId = 2,
                    Id = 4
                }
            };

        public List<object> PropertyImagesList => new List<object>()
            {
                new
                {
                    PropertyId = 1,
                    Url = "https://images.finncdn.no/dynamic/1280w/2019/10/vertical-2/09/4/159/957/514_879356192.jpg",
                    Id = 1
                },
                new
                {
                    PropertyId = 1,
                    Url = "https://images.finncdn.no/dynamic/1280w/2019/10/vertical-2/09/4/159/957/514_1858150189.jpg",
                    Id = 2
                },
                new
                {
                    PropertyId = 2,
                    Url = "https://images.finncdn.no/dynamic/1600w/2019/4/vertical-2/05/7/144/257/847_556478763.jpg",
                    Id = 3
                },
                new
                {
                    PropertyId = 2,
                    Url = "https://images.finncdn.no/dynamic/1600w/2019/4/vertical-2/05/7/144/257/847_1532635451.jpg",
                    Id = 4
                },
                new
                {
                    PropertyId = 2,
                    Url = "https://images.finncdn.no/dynamic/1600w/2019/4/vertical-2/05/7/144/257/847_1807189903.jpg",
                    Id = 5
                },
                new
                {
                    PropertyId = 3,
                    Url = "https://images.finncdn.no/dynamic/1600w/2019/9/vertical-2/18/5/149/492/785_669939480.jpg",
                    Id = 6
                },
                new
                {
                    PropertyId = 4,
                    Url = "https://images.finncdn.no/dynamic/1280w/2019/10/vertical-2/25/1/161/351/381_1844547171.jpg",
                    Id = 7
                },
                new
                {
                    PropertyId = 4,
                    Url = "https://images.finncdn.no/dynamic/1280w/2019/10/vertical-2/25/1/161/351/381_983293329.jpg",
                    Id = 8
                },
                new
                {
                    PropertyId = 5,
                    Url = "https://images.finncdn.no/dynamic/1600w/2019/8/vertical-2/15/0/146/907/100_179405212.jpg",
                    Id = 9
                },
                new
                {
                    PropertyId = 5,
                    Url = "https://images.finncdn.no/dynamic/1280w/2019/8/vertical-2/15/0/146/907/100_1830835313.jpg",
                    Id = 10
                }
            };

        public List<object> AccountsList
        {
            get
            {
                PasswordHasher<Account> ph = new PasswordHasher<Account>();
                var buyer = new Account
                {
                    UserName = "buyer@hotmail.com",
                    Email = "buyer@hotmail.com",
                    FirstName = "Mighty",
                    LastName = "Guy",
                    Birthday = DateTime.ParseExact("05/01/2004", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    CreatedAt = DateTime.UtcNow,
                    AccountType = AccountTypesList[1],
                    NormalizedEmail = "buyer@hotmail.com".ToUpper(),
                    NormalizedUserName = "buyer@hotmail.com".ToUpper(),
                    TwoFactorEnabled = false,
                    EmailConfirmed = true,
                    PhoneNumber = "123456789",
                    PhoneNumberConfirmed = false
                };

                buyer.PasswordHash = ph.HashPassword(buyer, "rocklee");

                var agent = new Account
                {
                    UserName = "smith@hotmail.com",
                    Email = "smith@hotmail.com",
                    FirstName = "Mr",
                    LastName = "Smith",
                    Birthday = DateTime.ParseExact("05/01/1999", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    CreatedAt = DateTime.UtcNow,
                    AccountType = AccountTypesList[0],
                    NormalizedEmail = "smith@hotmail.com".ToUpper(),
                    NormalizedUserName = "smith@hotmail.com".ToUpper(),
                    TwoFactorEnabled = false,
                    EmailConfirmed = true,
                    PhoneNumber = "987654321",
                    PhoneNumberConfirmed = false
                };
                agent.PasswordHash = ph.HashPassword(agent, "anderson");

                // Make them to anonymous objects
                var anonBuyer = new
                {
                    UserName = "buyer@hotmail.com",
                    Email = "buyer@hotmail.com",
                    FirstName = "Mighty",
                    LastName = "Guy",
                    Birthday = DateTime.ParseExact("05/01/2004", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    CreatedAt = DateTime.UtcNow,
                    AccountTypeId = AccountTypesList[1].Id,
                    NormalizedEmail = "buyer@hotmail.com".ToUpper(),
                    NormalizedUserName = "buyer@hotmail.com".ToUpper(),
                    TwoFactorEnabled = false,
                    EmailConfirmed = true,
                    PhoneNumber = "123456789",
                    PhoneNumberConfirmed = false,
                    buyer.PasswordHash,
                    Id = Guid.NewGuid().ToString(),
                    AccessFailedCount = 0,
                    LockoutEnabled = true,
                    SecurityStamp = string.Empty
                };

                var anonAgent = new
                {
                    UserName = "smith@hotmail.com",
                    Email = "smith@hotmail.com",
                    FirstName = "Mr",
                    LastName = "Smith",
                    Birthday = DateTime.ParseExact("05/01/1999", "d/MM/yyyy", CultureInfo.InvariantCulture),
                    CreatedAt = DateTime.UtcNow,
                    AccountTypeId = AccountTypesList[0].Id,
                    NormalizedEmail = "smith@hotmail.com".ToUpper(),
                    NormalizedUserName = "smith@hotmail.com".ToUpper(),
                    TwoFactorEnabled = false,
                    EmailConfirmed = true,
                    PhoneNumber = "987654321",
                    PhoneNumberConfirmed = false,
                    agent.PasswordHash,
                    Id = Guid.NewGuid().ToString(),
                    AccessFailedCount = 0,
                    LockoutEnabled = true,
                    SecurityStamp = string.Empty
                };

                return new List<object>() { anonBuyer, anonAgent };
            }
        }
    }
}
