﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Property_Project.Models.Seed
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            SeedData seedData = new SeedData();

            modelBuilder.Entity<AccountType>().HasData(
                    seedData.AccountTypesList                );
            modelBuilder.Entity<OwnerType>().HasData(
                    seedData.OwnerTypesList                );
            modelBuilder.Entity<PropertyType>().HasData(
                    seedData.PropertyTypesList                );
            modelBuilder.Entity<PropertyStatus>().HasData(
                    seedData.PropertyStatusList                );
            modelBuilder.Entity<Owner>().HasData(
                    seedData.OwnersList                );
            modelBuilder.Entity<Property>().HasData(
                    seedData.PropertiesList    );
            modelBuilder.Entity<OwnershipLog>().HasData(
                    seedData.OwnershipLogsList
                );
            modelBuilder.Entity<Renovation>().HasData(
                    seedData.RenovationsList
                );
            modelBuilder.Entity<Valuation>().HasData(
                    seedData.ValuationsList
                );
            modelBuilder.Entity<PropertyImage>().HasData(
                    seedData.PropertyImagesList
                );
            modelBuilder.Entity<Account>().HasData(
                    seedData.AccountsList
                );
        }
    }
}
