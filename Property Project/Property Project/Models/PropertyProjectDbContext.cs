﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Property_Project.Models.Seed;

namespace Property_Project.Models
{
    // DbContext class, using elements from identity
    public class PropertyProjectDbContext : IdentityDbContext
    {
        private readonly IConfiguration configuration;

        public PropertyProjectDbContext(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(configuration["ConnectionStrings:DefaultConnection"]);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Seed();
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountType> AccountTypes { get; set; }
        public DbSet<Owner> Owners { get; set; }
        public DbSet<OwnershipLog> OwnershipLogs { get; set; }
        public DbSet<OwnerType> OwnerTypes { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<PropertyImage> PropertyImages { get; set; }
        public DbSet<PropertyStatus> PropertyStatuses { get; set; }
        public DbSet<PropertyType> PropertyTypes { get; set; }
        public DbSet<Renovation> Renovations { get; set; }
        public DbSet<Valuation> Valuations { get; set; }
        public DbSet<RateLimit> RateLimits { get; set; }
    }
}
