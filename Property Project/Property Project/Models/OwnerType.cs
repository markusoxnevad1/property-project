﻿using System.Collections.Generic;

namespace Property_Project.Models
{
    public class OwnerType
    {
        // Class for what type of owner the property has
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Owner> Owners { get; set; }
    }
}
