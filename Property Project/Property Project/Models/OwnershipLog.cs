﻿using System;

namespace Property_Project.Models
{
    // Class for the logs of current and previous owners
    public class OwnershipLog
    {
        public int Id { get; set; }
        public DateTime DateAcquired { get; set; }
        public DateTime? DateSold { get; set; }
        public DateTime CreatedAt { get; set; }
        public Property Property { get; set; }
        public Owner Owner { get; set; }
    }
}
