﻿using System;

namespace Property_Project.Models
{
    // Class for rate limiting
    public class RateLimit
    {
        public int Id {get; set;}
        public string Ip { get; set; }
        public int LockoutCount { get; set; }
        public DateTime LockoutTime { get; set; }
    }
}
