﻿using System.Collections.Generic;

namespace Property_Project.Models
{
    // Class for the property types
    public class PropertyType
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Property> Properties { get; set; }
    }
}
