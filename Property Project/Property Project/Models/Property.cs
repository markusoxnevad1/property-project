﻿using System;
using System.Collections.Generic;

namespace Property_Project.Models
{
    // Property class
    public class Property
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Value { get; set; }
        public string ShortAddress { get; set; }
        public string LongAddress { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int Zip { get; set; }
        public DateTime BuiltAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public PropertyStatus PropertyStatus { get; set; }
        public PropertyType PropertyType { get; set; }

        public ICollection<OwnershipLog> OwnershipLogs { get; set; }
        public ICollection<Renovation> Renovations { get; set; }
        public ICollection<Valuation> Valuations { get; set; }
        public ICollection<PropertyImage> PropertyImages { get; set; }
    }
}
