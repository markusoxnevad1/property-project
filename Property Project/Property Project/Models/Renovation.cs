﻿using System;

namespace Property_Project.Models
{
    // Class for the renovation history of a property
    public class Renovation
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public Property Property { get; set; }
    }
}
