﻿import React, { Component } from "react"
import { PropertyComponent } from "../../components/property/PropertyComponent";

export class PropertyCatalogue extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            data: {},
            accountRole: "",
        }

        this.getAllProperties = this.getAllProperties.bind(this);
        this.getAccountType = this.getAccountType.bind(this);
    }

    componentDidMount() {
        this.getAllProperties();
        this.getAccountType();
    }

    render() {
        var properties = <div className="loader"></div>;
        if (Object.keys(this.state.data).length > 0) {
            properties = this.state.data.map(property => (
                <PropertyComponent
                    data={property}
                    key={property.id}
                    save={this.state.accountRole === "BUYER" ? true : false}
                />
                ));
        }

        return (
            <div className="row">
                {properties}
            </div>
        )
    }

    getAccountType() {
        var token = sessionStorage.getItem("usertoken");
        if (token) {
            var payload = JSON.parse(window.atob(token.split('.')[1]));
            this.setState({ accountRole: payload.role });
        }
    }

    async getAllProperties() {
        await fetch ("/api/property", {
            headers: { "Authorization": `Bearer ${sessionStorage.getItem("usertoken")}` }
            })
            .then(response => response.json())
            .then(data => this.setState({ data: data }));
    }
}

export default PropertyCatalogue;