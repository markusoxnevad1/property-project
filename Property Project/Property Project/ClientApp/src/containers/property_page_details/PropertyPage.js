﻿import React, { Component } from "react";
import PropertyDetails from "../../components/property_page_details/PropertyDetails";

export class PropertyPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            accountRole: "",
            data: {},
        }

        this.getAccountType = this.getAccountType.bind(this);
        this.getPropertyData = this.getPropertyData.bind(this);
    }

    componentDidMount() {
        this.getAccountType();
        this.getPropertyData();
    }

    getAccountType() {
        var token = sessionStorage.getItem("usertoken");
        if (token) {
            var payload = JSON.parse(window.atob(token.split('.')[1]));
            this.setState({ accountRole: payload.role });
        }
    }

    render() {
        var page = <div className="loader"></div>;

        if (Object.keys(this.state.data).length > 0) {
            page = <PropertyDetails data={this.state.data} role={this.state.accountRole} />
        }

        return (
            <>
                {page}
            </>
            )
    }

    async getPropertyData() {
        await fetch(
            "/api/property/" + this.props.match.params.id,
            {
                headers: { "Authorization": `Bearer ${sessionStorage.getItem("usertoken")}` }
            })
            .then(response => response.json())
            .then(data => this.setState({ data: data }));
    }
}

export default PropertyPage;