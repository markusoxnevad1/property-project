﻿import React from "react";
import { Redirect } from 'react-router-dom';


const withAuth = (Component) => {
    return class App extends Component {
        constructor(props) {
            super(props);
            this.state = {
                isLoading: true, 
                isAuthenticated: false,
            }
            this.UpdateAuthenticationState = this.UpdateAuthenticationState.bind(this);
        }

        componentDidMount() {
            this.UpdateAuthenticationState();
        }

        render() {
            const { isLoading, isAuthenticated } = this.state;
            if (isLoading) {
                return <div>Loading...</div>;
            }
            if (!isAuthenticated) {
                if (window.location.pathname !== "/settings" && window.location.pathname !== "/" && window.location.pathname !== "/changepassword") {
                    sessionStorage.setItem("redirect", window.location.pathname)
                }
                
                return <Redirect to="/" />;
            }
            return <Component {...this.props} />;
        }

        async UpdateAuthenticationState() {
            await fetch(
                "/authentication/verify",
                {
                    headers: { "Authorization": `Bearer ${sessionStorage.getItem("usertoken")}` }
                })
                .then(response => {
                    if (response.ok) { this.setState({ isAuthenticated: true, isLoading: false }) }
                    else { this.setState({ isLoading: false }) }
                })
        }
    }
}

export default withAuth;