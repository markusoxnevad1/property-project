import React, { Component } from 'react';
import { Route, withRouter } from 'react-router';
import { Layout } from './components/Layout';
import Login from './components/Login';
import { PropertyPage } from "./containers/property_page_details/PropertyPage";
import { PropertyCatalogue } from "./containers/property_catalogue/PropertyCatalogue";
import Settings from './components/Settings';
import ChangePassword from './components/ChangePassword';
import './custom.css'
import withAuth from "./ProtectedRoute"


export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <Layout>
                <Route exact path='/' component={withRouter(Login)} />
                <Route exact path='/settings' component={withRouter(withAuth(Settings))}/>
                <Route exact path='/changepassword' component={withRouter(withAuth(ChangePassword))}/>
                <Route path='/properties/:id' component={withRouter(withAuth(PropertyPage))}/>
                <Route exact path='/properties' component={withRouter(withAuth(PropertyCatalogue))}/>
            </Layout>
        );
    }
}
