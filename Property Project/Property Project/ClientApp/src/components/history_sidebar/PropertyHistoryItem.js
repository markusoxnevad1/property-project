﻿import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Card, CardHeader, CardBody, CardTitle, CardImg } from 'reactstrap';


class PropertyHistoryItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
        }

        this.getPropertyData = this.getPropertyData.bind(this);
    }

    componentDidMount() {
        this.getPropertyData();
    }

    render() {
        if (Object.keys(this.state.data).length === 0) {
            return (<p></p>)
        }
        return (
            <Card style={{ marginBottom: "1.6em" }}>
                <CardHeader>
                    <CardImg src={this.state.data.propertyImages[0]["url"]} />
                </CardHeader>
                <CardBody>
                    <CardTitle>
                        <Link className="ellipsis-1"
                            to={{ pathname: "/properties/" + this.state.data.id, }}>
                            {this.state.data.title}
                        </Link>
                    </CardTitle>
                </CardBody>
            </Card>
            )
    }

    async getPropertyData() {
        await fetch("/api/property/" + this.props.url.split("/").pop(), {
            headers: { "Authorization": `Bearer ${sessionStorage.getItem("usertoken")}` }
        })
            .then(response => response.json())
            .then(data => this.setState({ data: data }));
    }
}


export default PropertyHistoryItem;