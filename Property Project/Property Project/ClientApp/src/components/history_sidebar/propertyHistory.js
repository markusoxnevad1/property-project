﻿import React from "react";
import PropertyHistoryItem from "./PropertyHistoryItem";
import { slide as Menu } from "react-burger-menu";

import "./History.css";

function PropertyHistory() {
    var history = sessionStorage.getItem("propertyHistory")
    var items = null;
    if (history) {
        history = history.split(",");
        var filterHistory = history.filter((el) => { return el !== null && el !== "" });
        items = filterHistory.map(url => (
            <PropertyHistoryItem url={url} key={url.split("/").pop()} />
        ));
    }

    return (
        <Menu noOverlay disableAutoFocus>
            <h1 style={{ textAlign:"center", marginBottom:"20px" }}>History</h1>
            {items}
        </Menu>
        )
}

export default PropertyHistory;