﻿import React from 'react'
import { Component } from 'react';
import { Button, Label, Input} from 'reactstrap';
import { Redirect } from 'react-router-dom';
import { Alert } from 'reactstrap';

export default class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            Email: "",
            Password: "",
            Redirect: false,
            Alert: false
        };
        this.handleEmailChange = this.handleEmailChange.bind(this)
        this.handlePasswordChange = this.handlePasswordChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.ContinueAsGuest = this.ContinueAsGuest.bind(this)
        this.renderRedirect = this.renderRedirect.bind(this)
        this.checkRedirect = this.checkRedirect.bind(this)
    }

    handleEmailChange(e) {
        const input = e.target.value
        this.setState({ Email: input })
    }

    handlePasswordChange(e) {
        const input = e.target.value
        this.setState({ Password: input })
    }

    // Method for when the user press the continue as guest button
    // Gives guests a token and redirect them

    async ContinueAsGuest() {
         
        await fetch('authentication', {
            method: "GET",
        }).then(response => response.json())
            .then(token => {
                sessionStorage.setItem("usertoken", token)
            }); 
        this.setState({
            redirect: true
        })
    }

    // Method for when the user press the submit button
    // Gives the user an error if the email and password combination is incorrect
    // Gives the user a token and redirect them if the information is correct

    async handleSubmit() {
        var user = {
            Email: this.state.Email,
            Password: this.state.Password,
        }

        await fetch('authentication', {
            headers: {
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(user)
        }).then(response => {
            if (!response.ok) { throw response }
            return response.json()
        }).then(token => {
        sessionStorage.setItem("usertoken", token)
        })
        .catch (err => {
            this.setState({
                alert: true
            })
        })
        this.checkRedirect();
    }

    // Redirect users if they got a token in session

    checkRedirect() {
        if (sessionStorage.getItem("usertoken")) {
            this.setState({
                redirect: true
            })
        }
        else {
            this.setState({
                alert: true
            })
        }
    }

    // Redirect to previous session or to the properties page as default

    renderRedirect = () => {
        if (this.state.redirect) {
            if (sessionStorage.getItem("redirect") !== null) {
                let redir = sessionStorage.getItem("redirect");
                sessionStorage.removeItem("redirect");
                return <Redirect to={ redir } />
            }
            return <Redirect to='/properties' />
        }
    }

    render() {
        return (
            <div className="Login">
                {sessionStorage.getItem("usertoken") ? <Redirect to='/properties' /> : null}
                {this.state.alert ? <Alert color="warning"> Incorrect Email or Password </Alert> : null}
                <form className="login-form" onSubmit={this.handleSubmit}>
                    <Label>Email</Label>
                    <br/>
                    <Input
                        className="form-control"
                        type="email"
                        placeholder="Email"
                        value={this.state.email}
                        onChange={this.handleEmailChange}
                        required
                    /><br />
                    <Label>Password</Label>
                    <br />
                    <Input
                        className="form-control"
                        type="password"
                        placeholder="Password"
                        value={this.state.password}
                        onChange={this.handlePasswordChange}
                        required
                    /><br />
                    <Button className="btn btn-info form-control btn-block" onClick={this.handleSubmit} >Login </Button><br />
                </form>
                <div className="text-center">
                    {this.renderRedirect()}
                    <Button className="btn-continue-guest" onClick={this.ContinueAsGuest} > Continue as guest </Button>
                </div>
            </div>
        );
    }
}