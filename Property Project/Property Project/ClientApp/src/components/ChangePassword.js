﻿import React from 'react'
import { Component } from 'react';
import { Button, Label, Input } from 'reactstrap';
import { Alert } from 'reactstrap';
import { Redirect } from 'react-router-dom';

export default class ChangePassword extends Component {
    constructor(props) {
        super(props);

        this.state = {
            newPassword: "",
            oldPassword: "",
            isChanging: false,
            confirmPassword: "",
            alert: false,
            success: false,
            alertmessage: "Unknown Error"
        };
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.validateForm = this.validateForm.bind(this)
        this.getIdData = this.getIdData.bind(this)
        this.handleError = this.handleError.bind(this)
        this.getAccountType = this.getAccountType.bind(this)
    }

    getAccountType() {
        var token = sessionStorage.getItem("usertoken");
        if (token) {
            var payload = JSON.parse(window.atob(token.split('.')[1]));
            return payload.role;
        }
    }

    // Checking what errormessage to use if the update failed

    handleError() {
        this.setState({
            alert: true
        })
        if (!/\d/.test(this.state.newPassword)) {
            this.setState({
                alertmessage: "The password must contain atleast 1 digit"
            })
        }
        else {
            this.setState({
                alertmessage: "Incorrect old password"
            })
        }
    }

    // get the user id from the token stored in session

    getIdData() {
        var token = sessionStorage.getItem("usertoken");
        if (token) {
            var payload = JSON.parse(window.atob(token.split('.')[1]));
            return payload.unique_name;
        }
    }

    // method that disable clicking submit if the input is not valid

    validateForm() {
        return (
            this.state.newPassword.length > 5 &&
            this.state.newPassword === this.state.confirmPassword &&
            this.state.oldPassword.length > 0
        );
    }

    handleChange(evt) {
        this.setState({ [evt.target.name]: evt.target.value });
    }

    // Update the user password for the logged in user if the inputs are correct
    // Gives the user an update success or error message after the fetch.

    async handleSubmit() {
        this.setState({
            alert: false,
            success: false
        })
        const data = {
            oldPassword: this.state.oldPassword,
            newPassword: this.state.newPassword
        }
        this.setState({ isChanging: true });

        var id = this.getIdData();
        var token = sessionStorage.getItem("usertoken");
        await fetch('api/account/changepassword/' + id, {
            headers: !token ? {} : {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            method: "PUT",
            body: JSON.stringify(data)
        }).then(response => {
            if (response.ok) {
                this.setState({
                    success: true
                })
            }
            else {
                this.handleError();
            }
        })
    }

    render() {
        var role = this.getAccountType();
        return (
            <div className="ChangePassword">
                {(role === "AGENT" || role === "BUYER") ? null : <Redirect to='/properties' />}
                {this.state.alert ? <Alert color="warning"> {this.state.alertmessage} </Alert> : null}
                {this.state.success ? <Alert color="success"> Update successful </Alert> : null}
                <form className="changepassword-form" onSubmit={this.handleSubmit}>
                    <Label>Old Password</Label>
                    <br />
                    <Input
                        className="form-control"
                        name="oldPassword"
                        type="password"
                        onChange={this.handleChange}
                        required
                    /><br />
                    <Label>New Password</Label>
                    <br />
                    <Input
                        className="form-control"
                        name="newPassword"
                        type="password"
                        onChange={this.handleChange}
                        required
                    /><br />
                    <Label>Confirm Password</Label>
                    <br />
                    <Input
                        className="form-control"
                        name="confirmPassword"
                        type="password"
                        onChange={this.handleChange}
                        required
                    /><br />
                    <Button
                        className="btn-confirmpassword"
                        type="button"
                        disabled={!this.validateForm()}
                        onClick={this.handleSubmit}
                    >Confirm Password</Button>
                </form>
            </div>
        )
    }
}
