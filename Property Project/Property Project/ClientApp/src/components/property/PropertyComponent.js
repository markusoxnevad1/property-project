﻿import React from "react";
import { Link } from "react-router-dom";
import { Card, CardHeader, CardBody, CardTitle, CardText, CardFooter, CardImg } from 'reactstrap';

export function PropertyComponent(props) {
    var linkButton =
        <Link
            className="text-center btn btn-primary"
            to={{ pathname: "/properties/" + props.data.id, }}
            onClick={() => props.save ? addToSessionHistory(props.data.id) : null}>
            View
        </Link>

    return (
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-3 py-3 text-center">
            <Card className="h-100">
                <CardHeader>
                    <CardImg src={props.data.propertyImages[0]["url"]} />
                </CardHeader>
                <CardBody className="d-flex flex-column" >
                    <CardTitle className="ellipsis-2">
                        {props.data.title}
                    </CardTitle>
                    <CardText className="text-center mt-auto">
                        {props.data.shortAddress}
                        <br />
                        <em>{props.data.city}</em>
                    </CardText>
                </CardBody>
                <CardFooter className="w-100">
                    {linkButton}
                </CardFooter>
            </Card>
        </div>


    )
}

function addToSessionHistory(id) {
    var url = window.location.href + "/" + id;
    var history = sessionStorage.getItem("propertyHistory");

    if (history == null) {
        history = url + ",";
        sessionStorage.setItem("propertyHistory", history);
        return
    }

    history = history.split(",");

    if (!history.includes(url)) {
        if (history.length >= 3) {
            history.pop();
            history.unshift(url);
        } else {
            history.unshift(url);
        }
        sessionStorage.setItem("propertyHistory", history);
    }
}

export default PropertyComponent;