﻿import React, { Component } from 'react'
import { Map as LeafletMap, TileLayer, Marker, Popup } from 'react-leaflet';
import { OpenStreetMapProvider } from "leaflet-geosearch";

import "./Map.css"

const provider = new OpenStreetMapProvider();

class Map extends Component {
    constructor(props) {
        super(props);

        this.state = {
            coordinates: [],
        }

        this.fetchCoordinates = this.fetchCoordinates.bind(this);
    }

    componentDidMount() {
        this.fetchCoordinates();
    }

    fetchCoordinates() {
        provider.search({ query: this.props.address })
            .then(result => {
                this.setState({ coordinates: [result[0].y, result[0].x] })
            });
    }

    render() {
        if (this.state.coordinates.length > 0) {
            return (
                <LeafletMap
                    center={this.state.coordinates}
                    zoom={15}
                    maxZoom={15}
                    attributionControl={true}
                    zoomControl={true}
                    doubleClickZoom={true}
                    scrollWheelZoom={true}
                    dragging={true}
                    animate={true}
                    easeLinearity={0.35}
                >
                    <TileLayer
                        url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
                    />
                    <Marker position={this.state.coordinates}>
                        <Popup>
                            {this.props.address}
                        </Popup>
                    </Marker>
                </LeafletMap>
            );
        } else {
            return (
                "Loading..."
            );
        }
    }
}

export default Map