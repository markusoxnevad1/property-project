﻿import React from "react";
import { Row, UncontrolledCarousel } from 'reactstrap';
import { HistoryOwner, HistoryRenovation } from "./History";
import QRCode from "qrcode.react";
import Map from "./Map"
import moment from "moment"

export default function PropertyDetails(props) {
    var userType = props.role;

    var downloadQR = () => {
        const canvas = document.getElementById("249032132532");
        const pngUrl = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
        let downloadLink = document.createElement("a");
        downloadLink.href = pngUrl;
        downloadLink.download = props.data.title + ".png";
        downloadLink.click();
    };

    if (userType === "AGENT") {
        var ownerHistory = (
            <Row>
                <div className="col-12" style={{ marginTop: "50px" }}>
                    <h3>List of current and previous owners</h3>
                    <HistoryOwner ownershipLogs={props.data.ownershipLogs} />
                </div>
            </Row>
        )

        var renovationHistory = (
            <Row>
                <div className="col-12" style={{ marginTop: "50px" }}>
                    <h3>List renovations</h3>
                    <HistoryRenovation renovations={props.data.renovations} />
                </div>
            </Row>
        )
    }

    var carouselImages = props.data.propertyImages.map(image => {
        return {
            src: image.url,
            key: "img-key-" + image.id,
            altText: "An image",
            caption: '',
            header: '',
        }
    });

    return (
        <>
            <Row>
                <div className="col-lg-6 col-md-12">
                    <UncontrolledCarousel interval={false} items={carouselImages} />
                    <br />
                </div>
                <div className="col-lg-1 col-md-0"></div>

                <div className="col-lg-5 col-md-12">
                    <h1>{props.data.title}</h1>
                    <br />
                    <h2>{userType === "AGENT" ? "Price: " + new Intl.NumberFormat("de-DE", { style: "currency", currency: "NOK" }).format(props.data.currentPropertyValue) : null}</h2>
                    <p>{userType !== "Guest" ? props.data.longAddress : props.data.shortAddress}</p>
                    <br />
                    <p>{props.data.propertyType}</p>
                    <p>{userType !== "Guest" ? "Built: " + moment(props.data.builtAt).format("LL") : null}</p>
                    <p>{userType !== "Guest" ? "Renovated: " + moment(props.data.lastRenovation).format("LL") : null}</p>
                    <br />
                    <p>{userType === "AGENT" ? "Current owner: " + props.data.currentOwnerName : null}</p>
                    <p>{userType === "AGENT" ? "Owner tlf: " + props.data.currentOwnerTlf : null}</p>
                    <p>{userType === "AGENT" ? "Owner email: " + props.data.currentOwnerEmail : null}</p>

                    <div className="text-center">
                        <QRCode
                            value={window.location.href}
                            includeMargin={true}
                            id="249032132532"
                        />
                        <br />
                        <button onClick={downloadQR}> Download QR </button>
                    </div>
                </div>
            </Row>

            {userType === "AGENT" ? ownerHistory : null}
            {userType === "AGENT" ? renovationHistory : null}

            <Row>
                <div className="col-12">
                    <Map address={props.data.shortAddress} />
                </div>
            </Row>
        </>
        );
}