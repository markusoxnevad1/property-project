﻿import React from "react";
import { Table } from "reactstrap";
import moment from "moment";

export function HistoryOwner(props) {
    var owners = props.ownershipLogs.map(log => (
        <tr key={Math.random()}>
            <td>{log.firstName + " " + log.lastName}</td>
            <td>{moment(log.dateAcquired).format("LL")}</td>
            <td>{log.dateSold ? moment(log.dateSold).format("LL") : "Not sold"}</td>
            </tr>
        ));

    return (
        <Table striped>
            <thead>
                <tr key="1">
                    <th>Owner</th>
                    <th>Aquired</th>
                    <th>Sold</th>
                </tr>
            </thead>
            <tbody>
                {owners}
            </tbody>
        </Table>
        )
}

export function HistoryRenovation(props) {
    var renovations = props.renovations.map(renovation => (
        <tr key={Math.random()}>
            <td>{moment(renovation.dateFrom).format("LL")}</td>
            <td>{moment(renovation.dateTo).format("LL")}</td>
            <td style={{ wordBreak: "break-all" }}>{renovation.description}</td>
        </tr>
    ));

    return (
        <Table striped>
            <thead>
                <tr key="1">
                    <th>From</th>
                    <th>To</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                {renovations}
            </tbody>
        </Table>
    )
}