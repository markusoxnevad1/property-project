import React, { Component } from 'react';
import { Collapse, Container, Navbar, NavbarToggler, NavLink, NavbarBrand } from 'reactstrap';
import { Link } from 'react-router-dom';
import PropertyHistory from './history_sidebar/propertyHistory';
import './NavMenu.css';

export class NavMenu extends Component {
  static displayName = NavMenu.name;

    constructor(props) {
        super(props);

        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.getAccountType = this.getAccountType.bind(this);

        this.state = {
            collapsed: true
        };
    }

    componentDidMount() {
        this.getAccountType();
    }

    getAccountType() {
        var token = sessionStorage.getItem("usertoken");
        if (token) {
            var payload = JSON.parse(window.atob(token.split('.')[1]));
            return payload.role;
        }
    }

  toggleNavbar () {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

    render() {
        var history = null;

        var token = sessionStorage.getItem("usertoken");
        if (token) {
            var payload = JSON.parse(window.atob(token.split('.')[1]));
            if (payload.role === "BUYER") {
                history = <PropertyHistory />
            }
        }

        var role = this.getAccountType();
    return (
      <header>
            <Navbar className="navbar-expand-sm navbar-toggleable-sm ng-white border-bottom box-shadow mb-3" light>
            {history}
            <NavbarBrand tag={Link} className={"text-dark mx-auto"} to="/properties" >Property Catalogue</NavbarBrand>
            <NavLink tag={Link} className={(role === "AGENT" || role === "BUYER") ? 'text-dark' : 'hidden'} to="/settings">Settings</NavLink>
        </Navbar>
      </header>
    );
  }
}
