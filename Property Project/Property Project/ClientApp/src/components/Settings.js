﻿import React from 'react'
import { Component } from 'react';
import { Button, Label, Input } from 'reactstrap';
import { Redirect } from 'react-router-dom';
import { Alert } from 'reactstrap';

export default class Settings extends Component {
    static displayName = Settings.name;

    constructor(props) {
        super(props);
        this.state = {
            Email_tmp: "",
            FirstName_tmp: "",
            LastName_tmp: "",
            Phone_tmp: "",
            redirect: false,
            alert: false,
            success: false,
            alertmessage: "Unknown Error"
        };
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.redirectchangepassword = this.redirectchangepassword.bind(this)
        this.getuserinfo = this.getuserinfo.bind(this)
        this.getIdData = this.getIdData.bind(this)
        this.validSubmit = this.validSubmit.bind(this)
        this.getAccountType = this.getAccountType.bind(this)
    }

    getAccountType() {
        var token = sessionStorage.getItem("usertoken");
        if (token) {
            var payload = JSON.parse(window.atob(token.split('.')[1]));
            return payload.role;
        }
    }

    // Check if the email input is a valid email

    validSubmit() {
        if (!/\S+@\S+\.\S+/.test(this.state.Email_tmp)) {
            this.setState({
                alert: true,
                success: false,
                alertmessage: "Invalid email input"
            })
            return false;
        }
        if (this.state.Email_tmp.length < 1) {
            this.setState({
                alert: true,
                success: false,
                alertmessage: "Invalid Email input"
            })
            return false;
        }
        if (this.state.FirstName_tmp.length < 1) {
            this.setState({
                alert: true,
                success: false,
                alertmessage: "Invalid FirstName input"
            })
            return false;
        }
        if (this.state.LastName_tmp.length < 1) {
            this.setState({
                alert: true,
                success: false,
                alertmessage: "Invalid LastName input"
            })
            return false;
        }
        if (this.state.Phone_tmp.toString().length < 1) {
            this.setState({
                alert: true,
                success: false,
                alertmessage: "Invalid Phone input"
            })
            return false;
        }
        return true;
    }

    // redirect to the changepassword page when the user click change password

    redirectchangepassword() {
        this.setState({
            redirect: true
        })
    }

    // get the user id from the token stored in session

    getIdData() {
        var token = sessionStorage.getItem("usertoken");
        if (token) {
            var payload = JSON.parse(window.atob(token.split('.')[1]));
            return payload.unique_name;
        }
    }

    // get the user info for the logged in user

    async getuserinfo() {
        var id = this.getIdData();
        fetch("/api/account/" + id,
            {
                headers: { "Authorization": `Bearer ${sessionStorage.getItem("usertoken")}` }
            })
            .then(response => response.json())
            .then(data => this.setState({
                Email_tmp: data.email,
                FirstName_tmp: data.firstName,
                LastName_tmp: data.lastName,
                Phone_tmp: data.phone
            }));
    }

    componentDidMount() {
        this.getuserinfo();
    }
    
    handleChange(evt) {
        this.setState({ [evt.target.name]: evt.target.value });
    }

    // Update the user info for the logged in user
    // Gives the user an update success or error message after the fetch.

    async handleSubmit() {
        if (this.validSubmit()) {
            this.setState({
                alert: false,
                success: false
            })
            const data = {
                email: this.state.Email_tmp,
                firstName: this.state.FirstName_tmp,
                lastName: this.state.LastName_tmp,
                phone: this.state.Phone_tmp
            }
            var id = this.getIdData();
            var token = sessionStorage.getItem("usertoken");
            await fetch('api/account/' + id, {
                headers: !token ? {} : {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                method: "PUT",
                body: JSON.stringify(data)
            }).then(response => {
                if (response.ok) {
                    this.setState({
                        success: true
                    })
                }
                else {
                    this.setState({
                        alert: true,
                        alertmessage: "Error"
                    })
                }
            })
        }
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/changepassword' />
        }
    }

    render() {
        var role = this.getAccountType();
    return (
        <div className="Settings">
            {(role === "AGENT" || role === "BUYER") ? null : <Redirect to='/properties' />}
            {this.state.alert ? <Alert color="warning"> {this.state.alertmessage} </Alert> : null}
            {this.state.success ? <Alert color="success"> Update successful </Alert> : null}
            <form className="settings-form" onSubmit={this.handleSubmit}>
                <Label>Email*</Label>
                <br />
                <Input
                    className="form-control"
                    name="Email_tmp"
                    type="email"
                    value={this.state.Email_tmp}
                    onChange={this.handleChange}
                    required
                /><br />
                <Label>FirstName</Label>
                <br />
                <Input
                    className="form-control"
                    name="FirstName_tmp"
                    type="text"
                    value={this.state.FirstName_tmp}
                    onChange={this.handleChange}
                    required
                /><br />
                <Label>LastName</Label>
                <br />
                <Input
                    className="form-control"
                    name="LastName_tmp"
                    type="text"
                    value={this.state.LastName_tmp}
                    onChange={this.handleChange}
                    required
                /><br />
                <Label>Phone</Label>
                <br />
                <Input
                    className="form-control"
                    name="Phone_tmp"
                    type="number"
                    value={this.state.Phone_tmp}
                    onChange={this.handleChange}
                    required
                /><br />
                <small>*Changing the email will not change the email used at login</small><br />
                <Button className="btn-save-setting"
                    type="button"
                    onClick={this.handleSubmit}
                >Save changes</Button><br />
            </form>
            <br />
            <div className="text-center">
                {this.renderRedirect()}
                <Button
                    type="submit"
                    className="text-center btn-changepassword"
                    onClick={this.redirectchangepassword}
                >Change Password</Button>
            </div>
        </div>
        );
    }
}
