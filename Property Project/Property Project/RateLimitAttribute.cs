﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Property_Project.Models;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net;

namespace Property_Project
{
    [AttributeUsage(AttributeTargets.Method)]
    public class RateLimitAttribute : ActionFilterAttribute
    {
        private readonly PropertyProjectDbContext _context;
        private readonly int _lockoutTime = 5;

        public RateLimitAttribute(PropertyProjectDbContext context)
        {
            _context = context;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            // Check and possibly get the entry using the Ip address of the user
            var incommingIpAddress = context.HttpContext.Request.HttpContext.Connection.RemoteIpAddress.ToString();
            var userRateLimitEntry = _context.RateLimits.FirstOrDefault(rate => rate.Ip == incommingIpAddress);


            if (userRateLimitEntry == null)
            {

                // Create a new entry for the user and let the user pass
                _context.RateLimits.Add(new RateLimit()
                {
                    Ip = incommingIpAddress,
                    LockoutCount = 0,
                    LockoutTime = DateTime.UtcNow
                });
                _context.SaveChanges();
            }
            else
            {
                // Reset the counter if more than N minutes have passed since first request.
                if ((DateTime.UtcNow - userRateLimitEntry.LockoutTime).TotalMinutes >= _lockoutTime)
                {
                    userRateLimitEntry.LockoutCount = 0;
                    userRateLimitEntry.LockoutTime = DateTime.UtcNow;
                    _context.RateLimits.Update(userRateLimitEntry);
                    _context.SaveChanges();
                }
                else
                {
                    // Check if lockout count is more than the threshhold, for now 5 fails
                    if (userRateLimitEntry.LockoutCount >= 5)
                    {
                        // Too many requests
                        context.Result = new ContentResult
                        {
                            Content = $"Too many requests, please wait { _lockoutTime } minutes"
                        };
                        context.HttpContext.Response.StatusCode = (int)HttpStatusCode.TooManyRequests;
                    }
                }
            }
        }
    }
}
