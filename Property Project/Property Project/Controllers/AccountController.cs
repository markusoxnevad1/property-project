﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Property_Project.Models;
using Microsoft.AspNetCore.Mvc;
using Property_Project.Models.DTO;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace Property_Project.Controllers
{
    // Controller for handling accounts and account settings
    [Route("api/[controller]")]
    // Restricts the route for guests
    [Authorize(AuthenticationSchemes = "Bearer", Policy = "IsAuthenticated")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly PropertyProjectDbContext _context;
        private readonly UserManager<Account> _userManager;
        public AccountController(PropertyProjectDbContext context, UserManager<Account> userManager)
        {
            this._context = context;
            this._userManager = userManager;
        }
        // Route for retrieving account information by id
        [HttpGet("{id}")]
        public ActionResult<Account> GetAccount(string id)
        {
            Response.ContentType = "application/json";
            // Retrieves the id from the token
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            var tokenId = identity.FindFirst("unique_name");
            if (id == tokenId.Value)
            {
                Account user = _context.Accounts.Find(id);
                AccountInfoDto userInfo = new AccountInfoDto
                {
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Phone = Int32.Parse(user.PhoneNumber),
                };
                return Ok(userInfo);
            }
            return Forbid();
        }
        // Route for changing account information
        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateAccount(string id, AccountInfoDto account)
        {
            Response.ContentType = "application/json";
            // Retrieves the id from the token
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            var tokenId = identity.FindFirst("unique_name");
            if (id == tokenId.Value)
            {
                if (id != _context.Accounts.Find(id).Id)
                {
                    return BadRequest();
                }

                var userInfo = await _userManager.FindByIdAsync(id);
                userInfo.FirstName = account.FirstName;
                userInfo.LastName = account.LastName;
                userInfo.Email = account.Email;
                userInfo.PhoneNumber = account.Phone.ToString();

                await _userManager.UpdateAsync(userInfo);

                return NoContent();
            }
            return Forbid();
        }
        // Route for changing the password of the logged in account
        [HttpPut("changepassword/{id}")]
        public async Task<ActionResult> UpdatePassword(string id, AccountPasswordDto account)
        {
            Response.ContentType = "application/json";

            // Checks to see if there is any users in the db matching the id from the token
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            // Retrieves the id from the token
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            var tokenId = identity.FindFirst("unique_name");
            if (id == tokenId.Value)
            {
                var login = await _userManager.CheckPasswordAsync(user, account.OldPassword);
                if (login)
                {
                    var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                    var result = await _userManager.ResetPasswordAsync(user, token, account.NewPassword);
                    if (result.Succeeded == true)
                    {
                        return Ok(result);
                    }
                    else
                    {
                        return BadRequest(result);
                    }
                }
                return BadRequest();
            }
            return Forbid();
        }
    }
}