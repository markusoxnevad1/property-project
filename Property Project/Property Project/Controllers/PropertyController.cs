﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Property_Project.Models;
using Property_Project.Models.DTO;

namespace Property_Project.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiController]
    public class PropertyController : ControllerBase
    {
        private readonly PropertyProjectDbContext _context;

        public PropertyController(
            PropertyProjectDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<PropertyCardInfo> GetAllPropertyCardData()
        {
            var properties = _context.Properties.Include(prop => prop.PropertyImages).ToList();

            var propertyCardDetail = properties
                .Select(prop => new PropertyCardInfo 
                { Id = prop.Id, 
                  Title = prop.Title,
                  ShortAddress = prop.ShortAddress,
                  // Select only the required data. Allowing the FK would result in callback loop
                  PropertyImages =  prop.PropertyImages.Select(img => new { img.Id, img.Url }).ToList(),
                  City = prop.City
                });

            Response.ContentType = "application/json";

            return Ok(propertyCardDetail);
        }

        [HttpGet("{id}")]
        public ActionResult<GuestPropertyDto> GetOneProperty(int id)
        {
            // Find the current user role from the token
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            var role = identity.FindFirst("role");

            var property = new GuestPropertyDto();

            if (role.Value == "Guest")
            {
                property = GenerateGuestPropertyDto(id);
            } else if (role.Value == "BUYER") 
            {
                property = GenerateBuyerPropertyDto(id);
            } else if (role.Value == "AGENT") 
            {
                property = GenerateAgentPropertyDto(id);
            } else
            {
                return BadRequest();
            }

            Response.ContentType = "application/json";

            return Ok(property);
        }

        private GuestPropertyDto GenerateGuestPropertyDto(int id)
        {
            // Include will eager load related data
            var property = _context.Properties
                .Include(prop => prop.PropertyImages)
                .Include(prop => prop.PropertyType)
                .SingleOrDefault(prop => prop.Id == id);
            
            if (property == null)
            {
                return null;
            }
            
            GuestPropertyDto prop = new GuestPropertyDto()
            {
                Id = property.Id,
                Title = property.Title,
                ShortAddress = property.ShortAddress,
                LongAddress = property.LongAddress,
                PropertyImages = property.PropertyImages.Select(img => new { img.Id, img.Url }).ToList(),
                PropertyType = property.PropertyType.Name
            };
            
            return prop;
        }

        private BuyerPropertyDto GenerateBuyerPropertyDto(int id)
        {
            // Include will eager load related data
            var property = _context.Properties
                .Include(prop => prop.PropertyImages)
                .Include(prop => prop.Renovations)
                .Include(prop => prop.PropertyType)
                .SingleOrDefault(prop => prop.Id == id);
            
            if (property == null)
            {
                return null;
            }
            
            BuyerPropertyDto prop = new BuyerPropertyDto()
            {
                Id = property.Id,
                Title = property.Title,
                ShortAddress = property.ShortAddress,
                LongAddress = property.LongAddress,
                PropertyImages = property.PropertyImages.Select(img => new { img.Id, img.Url }).ToList(),
                City = property.City,
                Zip = property.Zip,
                BuiltAt = property.BuiltAt,
                LastRenovation = property.Renovations.Max(ren => ren.DateTo),
                PropertyType = property.PropertyType.Name
            };
            
            return prop;
        }

        private AgentPropertyDto GenerateAgentPropertyDto(int id)
        {
            // Include will eager load related data
            var property = _context.Properties
                .Include(prop => prop.PropertyImages)
                .Include(prop => prop.Renovations)
                .Include(prop => prop.PropertyType)
                .Include(prop => prop.OwnershipLogs)
                    .ThenInclude(log => log.Owner)
                .SingleOrDefault(prop => prop.Id == id);
            
            if (property == null)
            {
                return null;
            }

            var currOwner = property.OwnershipLogs.OrderBy(log => log.DateAcquired).LastOrDefault().Owner;


            AgentPropertyDto prop = new AgentPropertyDto()
            {
                Id = property.Id,
                Title = property.Title,
                ShortAddress = property.ShortAddress,
                LongAddress = property.LongAddress,
                PropertyImages = property.PropertyImages.Select(img => new { img.Id, img.Url }).ToList(),
                City = property.City,
                Zip = property.Zip,
                BuiltAt = property.BuiltAt,
                LastRenovation = property.Renovations.Max(ren => ren.DateTo),
                OwnershipLogs = property.OwnershipLogs.Select(log => new { log.Id, log.DateAcquired, log.DateSold, log.Owner.FirstName, log.Owner.LastName }).ToList(),
                CurrentOwnerName = currOwner.FirstName + " " + currOwner.LastName,
                CurrentOwnerEmail = currOwner.Email,
                CurrentOwnerTlf = currOwner.Phone,
                CurrentPropertyValue = property.Value,
                Renovations = property.Renovations.Select(ren => new { ren.Id, ren.Description, ren.DateFrom, ren.DateTo }).ToList(),
                PropertyType = property.PropertyType.Name
            };
            
            return prop;
        }
    }
}