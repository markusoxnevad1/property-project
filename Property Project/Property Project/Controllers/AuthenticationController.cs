﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Property_Project.Models;
using Property_Project.Models.DTO;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using System.Diagnostics;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Property_Project.Controllers
{
    
    [Route("[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly PropertyProjectDbContext _context;
        private readonly IConfiguration _configuration;
        private readonly UserManager<Account> _userManager;
        private readonly SignInManager<Account> _signInManager;

        public AuthenticationController(
            IConfiguration configuration,
            PropertyProjectDbContext context,
            UserManager<Account> userManager,
            SignInManager<Account> signInManager)
        {
            _configuration = configuration;
            this._userManager = userManager;
            this._signInManager = signInManager;
            this._context = context;
        }

        [AllowAnonymous]
        [ServiceFilter(typeof(RateLimitAttribute))]
        [HttpPost]
        public async Task<ActionResult<string>> Login([FromBody] AccountDto account)
        {
            Response.ContentType = "application/json";
            var result = await _signInManager.PasswordSignInAsync(account.Email, account.Password, false, false);

            if(result.Succeeded)
            {
                Account user = _userManager.Users.Include("AccountType").SingleOrDefault(user => user.UserName == account.Email);

                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_configuration["Secret"]);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Issuer = null,
                    Audience = null,
                    IssuedAt = DateTime.UtcNow,
                    NotBefore = DateTime.UtcNow,
                    
                    // Will reduce this if we implement refresh tokens
                    Expires = DateTime.UtcNow.AddDays(7),

                    // Define what is contained in the payload. Here is user id and role
                    Subject = new ClaimsIdentity(new List<Claim> {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.AccountType.Name)
                }),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var jwtToken = tokenHandler.CreateJwtSecurityToken(tokenDescriptor);
                var token = tokenHandler.WriteToken(jwtToken);

                return Ok(token);
            } else
            {
                // Find the user in the database. This should have been created in the middleware
                var userRateLimitEntry = await _context.RateLimits
                    .FirstOrDefaultAsync(rate => rate.Ip == Request.HttpContext.Connection.RemoteIpAddress.ToString());

                // Increate the lockout counter, which is checked in the rate limit attribute
                userRateLimitEntry.LockoutCount += 1;
                _context.SaveChanges();

                return Unauthorized("Invalid username and/or password");
            }
        }
        
        [AllowAnonymous]
        [HttpGet]
        public ActionResult<string> GuestLogin()
        {
            Response.ContentType = "application/json";
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_configuration["Secret"]);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = null,
                Audience = null,
                IssuedAt = DateTime.UtcNow,
                NotBefore = DateTime.UtcNow,
                Expires = DateTime.UtcNow.AddDays(7),
                Subject = new ClaimsIdentity(new List<Claim> { 
                    new Claim(ClaimTypes.Role, "Guest")
                }),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var jwtToken = tokenHandler.CreateJwtSecurityToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(jwtToken);

            return Ok(token);
        }

        [HttpGet("verify")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public ActionResult VerifyToken()
        {
            return Ok();
        }
    }
}