﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Property_Project.Migrations
{
    public partial class PropertyBuiltAtColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "BuiltAt",
                table: "Properties",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BuiltAt",
                table: "Properties");
        }
    }
}
