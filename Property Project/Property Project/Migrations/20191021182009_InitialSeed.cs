﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Property_Project.Migrations
{
    public partial class InitialSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AccountTypes",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "AGENT" },
                    { 2, "BUYER" }
                });

            migrationBuilder.InsertData(
                table: "OwnerTypes",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "COMPANY" },
                    { 2, "PRIVATE" }
                });

            migrationBuilder.InsertData(
                table: "PropertyStatuses",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "ON_MARKET" },
                    { 2, "AVAILABLE" },
                    { 3, "UNDER_RENOVATION" },
                    { 4, "UNAVAILABLE" },
                    { 5, "HISTORICAL" }
                });

            migrationBuilder.InsertData(
                table: "PropertyTypes",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "COMMERCIAL" },
                    { 2, "APARTMENT" },
                    { 3, "SEMI_DETACHED" },
                    { 4, "HOUSE" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Discriminator", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "AccountTypeId", "Birthday", "CreatedAt", "FirstName", "LastName" },
                values: new object[,]
                {
                    { "a83cb535-21d1-46f3-af84-70a3b8405cb3", 0, null, "Account", "smith@hotmail.com", true, true, null, "SMITH@HOTMAIL.COM", "SMITH@HOTMAIL.COM", "AQAAAAEAACcQAAAAEJ6fCgl+1jur7A56X5ESQaf/9ZCf/SBR/SFWzmgz7/QMNdc/vV8pl1/lXr5Hn6mK2A==", "987654321", false, "", false, "smith@hotmail.com", 1, new DateTime(1999, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 10, 21, 18, 20, 8, 903, DateTimeKind.Utc).AddTicks(9103), "Mr", "Smith" },
                    { "21a1ff14-8a62-4eb6-b3a9-5edb632e06e5", 0, null, "Account", "buyer@hotmail.com", true, true, null, "BUYER@HOTMAIL.COM", "BUYER@HOTMAIL.COM", "AQAAAAEAACcQAAAAECbumLhYF0rV3eEJfr8zkrrEN+u7oJnNPFpwJdFJTaVjCRdgNpaDrFTaFZcCT+cLOw==", "123456789", false, "", false, "buyer@hotmail.com", 2, new DateTime(2004, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 10, 21, 18, 20, 8, 903, DateTimeKind.Utc).AddTicks(5532), "Mighty", "Guy" }
                });

            migrationBuilder.InsertData(
                table: "Owners",
                columns: new[] { "Id", "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "OwnerTypeId", "Phone", "SocialSecurityNumber" },
                values: new object[,]
                {
                    { 2, new DateTime(1967, 12, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 10, 21, 18, 20, 8, 886, DateTimeKind.Utc).AddTicks(8405), "steven.segal@hotmail.com", "Steven", "Segal", 1, 98323531, 843123 },
                    { 1, new DateTime(1987, 4, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 10, 21, 18, 20, 8, 886, DateTimeKind.Utc).AddTicks(5898), "bob.ross@yahoo.com", "Bob", "Ross", 2, 92514212, 231312 }
                });

            migrationBuilder.InsertData(
                table: "Properties",
                columns: new[] { "Id", "BuiltAt", "City", "Country", "CreatedAt", "LongAddress", "PropertyStatusId", "PropertyTypeId", "ShortAddress", "Title", "Value", "Zip" },
                values: new object[,]
                {
                    { 3, new DateTime(2011, 7, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sola", "Norway", new DateTime(2019, 10, 21, 18, 20, 8, 887, DateTimeKind.Utc).AddTicks(6022), "Kjerrberg terrasse 7, 4051 Sola", 4, 3, "Kjerrberg terrasse 7, Sola", "NEW PRICE | Nice apartment on the top of Skadberg close to the previous Norwegian chess champion! Parking in a closed garagefacility", 1295000, 4051 },
                    { 1, new DateTime(2019, 8, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sandnes", "Norway", new DateTime(2019, 10, 21, 18, 20, 8, 887, DateTimeKind.Utc).AddTicks(3973), "Gryvjene 22, 4332 Figgjo", 2, 4, "Gryvjene 22, Figgjo", "FIGGJO - Large, copious house for sale | 2 kitchens | 3 bathrooms | 3 livingrooms | 7 bedrooms", 3250000, 4332 },
                    { 2, new DateTime(2020, 2, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sandnes", "Norway", new DateTime(2019, 10, 21, 18, 20, 8, 887, DateTimeKind.Utc).AddTicks(5958), "Granliveien, 4332 Figgjo", 2, 4, "Granliveien, Figgjo", "Rossåstoppen, Figgjo - 4 detached houses for sale | 2 livingrooms | 4 bedrooms | 2 bathrooms", 4390000, 4332 }
                });

            migrationBuilder.InsertData(
                table: "OwnershipLogs",
                columns: new[] { "Id", "CreatedAt", "DateAcquired", "DateSold", "OwnerId", "PropertyId" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 10, 21, 18, 20, 8, 888, DateTimeKind.Utc).AddTicks(3504), new DateTime(2011, 7, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2016, 8, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, 3 },
                    { 2, new DateTime(2019, 10, 21, 18, 20, 8, 888, DateTimeKind.Utc).AddTicks(5038), new DateTime(2016, 8, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, 3 },
                    { 4, new DateTime(2019, 10, 21, 18, 20, 8, 888, DateTimeKind.Utc).AddTicks(6773), new DateTime(2020, 2, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 2, 2 },
                    { 3, new DateTime(2019, 10, 21, 18, 20, 8, 888, DateTimeKind.Utc).AddTicks(6706), new DateTime(2019, 8, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 2, 1 }
                });

            migrationBuilder.InsertData(
                table: "PropertyImages",
                columns: new[] { "Id", "PropertyId", "Url" },
                values: new object[,]
                {
                    { 5, 2, "https://images.finncdn.no/dynamic/1600w/2019/4/vertical-2/05/7/144/257/847_1807189903.jpg" },
                    { 4, 2, "https://images.finncdn.no/dynamic/1600w/2019/4/vertical-2/05/7/144/257/847_1532635451.jpg" },
                    { 3, 2, "https://images.finncdn.no/dynamic/1600w/2019/4/vertical-2/05/7/144/257/847_556478763.jpg" },
                    { 1, 1, "https://images.finncdn.no/dynamic/1280w/2019/10/vertical-2/09/4/159/957/514_879356192.jpg" },
                    { 2, 1, "https://images.finncdn.no/dynamic/1280w/2019/10/vertical-2/09/4/159/957/514_1858150189.jpg" },
                    { 6, 3, "https://images.finncdn.no/dynamic/1600w/2019/9/vertical-2/18/5/149/492/785_669939480.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Renovations",
                columns: new[] { "Id", "DateFrom", "DateTo", "Description", "PropertyId" },
                values: new object[,]
                {
                    { 5, new DateTime(2019, 10, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 12, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "Added 2 new bedrooms, and built a new bathroom", 2 },
                    { 3, new DateTime(2019, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 7, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "Extended north side of the house, in order to build a new livingroom, also added an extra bathroom", 1 },
                    { 4, new DateTime(2019, 5, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 7, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "Built two new bedrooms", 1 },
                    { 2, new DateTime(2015, 9, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2015, 12, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Built a new parking facility", 3 },
                    { 1, new DateTime(2012, 5, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2012, 6, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "Built new bedroom", 3 }
                });

            migrationBuilder.InsertData(
                table: "Valuations",
                columns: new[] { "Id", "Comments", "PropertyId", "ValuationDate", "Value" },
                values: new object[,]
                {
                    { 3, "1 kitchen, 2 livingrooms, 4 bedrooms, 2 bathroom", 1, new DateTime(2019, 7, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 3250000 },
                    { 1, "1 kitchen, 1 bedroom, 1 bathroom", 3, new DateTime(2011, 6, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), 700000 },
                    { 2, "1 kitchen, 2 bedroom, 1 bathroom, 1 parking", 3, new DateTime(2016, 7, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 1295000 },
                    { 4, "2 kitchens, 3 bathrooms, 3 livingrooms, 7 bedrooms", 2, new DateTime(2020, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 4390000 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "21a1ff14-8a62-4eb6-b3a9-5edb632e06e5");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a83cb535-21d1-46f3-af84-70a3b8405cb3");

            migrationBuilder.DeleteData(
                table: "OwnershipLogs",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "OwnershipLogs",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "OwnershipLogs",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "OwnershipLogs",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PropertyImages",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PropertyImages",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PropertyImages",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PropertyImages",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PropertyImages",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PropertyImages",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PropertyStatuses",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PropertyStatuses",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PropertyStatuses",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PropertyTypes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PropertyTypes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Renovations",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Renovations",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Renovations",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Renovations",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Renovations",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Valuations",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Valuations",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Valuations",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Valuations",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "AccountTypes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "AccountTypes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "OwnerTypes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "OwnerTypes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PropertyStatuses",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PropertyStatuses",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PropertyTypes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PropertyTypes",
                keyColumn: "Id",
                keyValue: 4);
        }
    }
}
