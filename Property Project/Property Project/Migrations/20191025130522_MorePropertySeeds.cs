﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Property_Project.Migrations
{
    public partial class MorePropertySeeds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "21a1ff14-8a62-4eb6-b3a9-5edb632e06e5");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "a83cb535-21d1-46f3-af84-70a3b8405cb3");

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Discriminator", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "AccountTypeId", "Birthday", "CreatedAt", "FirstName", "LastName" },
                values: new object[,]
                {
                    { "cb097e14-8133-444b-b493-fa0dff0417d2", 0, null, "Account", "buyer@hotmail.com", true, true, null, "BUYER@HOTMAIL.COM", "BUYER@HOTMAIL.COM", "AQAAAAEAACcQAAAAEEPykfBHlmgzNfzhjt3sya75w5+OOfR+VAgcMZVmnRv9SgDwfr3PXcGXdGiyywlDfA==", "123456789", false, "", false, "buyer@hotmail.com", 2, new DateTime(2004, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 10, 25, 13, 5, 21, 698, DateTimeKind.Utc).AddTicks(2918), "Mighty", "Guy" },
                    { "f352b769-cfc1-4224-b9e1-1bb0b2467d8b", 0, null, "Account", "smith@hotmail.com", true, true, null, "SMITH@HOTMAIL.COM", "SMITH@HOTMAIL.COM", "AQAAAAEAACcQAAAAELurN5C3eX8LyhFuXVqHqPGgCU4ATn1EKHzWgt8h6qdG6OoPMtQgU5CMYkrjA6yO+g==", "987654321", false, "", false, "smith@hotmail.com", 1, new DateTime(1999, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 10, 25, 13, 5, 21, 698, DateTimeKind.Utc).AddTicks(6883), "Mr", "Smith" }
                });

            migrationBuilder.UpdateData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 25, 13, 5, 21, 685, DateTimeKind.Utc).AddTicks(3724));

            migrationBuilder.UpdateData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 25, 13, 5, 21, 685, DateTimeKind.Utc).AddTicks(5483));

            migrationBuilder.UpdateData(
                table: "OwnershipLogs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 25, 13, 5, 21, 686, DateTimeKind.Utc).AddTicks(9221));

            migrationBuilder.UpdateData(
                table: "OwnershipLogs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 25, 13, 5, 21, 687, DateTimeKind.Utc).AddTicks(572));

            migrationBuilder.UpdateData(
                table: "OwnershipLogs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 25, 13, 5, 21, 687, DateTimeKind.Utc).AddTicks(1684));

            migrationBuilder.UpdateData(
                table: "OwnershipLogs",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 25, 13, 5, 21, 687, DateTimeKind.Utc).AddTicks(1714));

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 25, 13, 5, 21, 686, DateTimeKind.Utc).AddTicks(1101));

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 25, 13, 5, 21, 686, DateTimeKind.Utc).AddTicks(2733));

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 25, 13, 5, 21, 686, DateTimeKind.Utc).AddTicks(2770));

            migrationBuilder.InsertData(
                table: "Properties",
                columns: new[] { "Id", "BuiltAt", "City", "Country", "CreatedAt", "LongAddress", "PropertyStatusId", "PropertyTypeId", "ShortAddress", "Title", "Value", "Zip" },
                values: new object[,]
                {
                    { 4, new DateTime(2019, 8, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Bergen", "Norway", new DateTime(2019, 10, 25, 13, 5, 21, 686, DateTimeKind.Utc).AddTicks(2776), "Telthussmauet 6B, 5003 Bergen", 1, 2, "Telthussmauet 6B, Bergen", "Skansen - Nyoppført enebolig med lekre kvaliteter og meget attraktiv beliggenhet.", 4600000, 5003 },
                    { 5, new DateTime(2019, 8, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Bergen", "Norway", new DateTime(2019, 10, 25, 13, 5, 21, 686, DateTimeKind.Utc).AddTicks(2780), "Amalie Skrams vei 16, 5036 Bergen", 3, 4, "Amalie Skrams vei 16, Bergen", "Sandviken - Pen 3-roms leilighet med stor og solrik balkong. Meget attraktiv beliggenhet. IN-ordning.", 2700000, 5036 }
                });

            migrationBuilder.InsertData(
                table: "OwnershipLogs",
                columns: new[] { "Id", "CreatedAt", "DateAcquired", "DateSold", "OwnerId", "PropertyId" },
                values: new object[,]
                {
                    { 5, new DateTime(2019, 10, 25, 13, 5, 21, 687, DateTimeKind.Utc).AddTicks(1720), new DateTime(2011, 7, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2016, 8, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, 4 },
                    { 6, new DateTime(2019, 10, 25, 13, 5, 21, 687, DateTimeKind.Utc).AddTicks(1739), new DateTime(2016, 8, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, 4 },
                    { 7, new DateTime(2019, 10, 25, 13, 5, 21, 687, DateTimeKind.Utc).AddTicks(1746), new DateTime(2011, 7, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2016, 8, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, 5 },
                    { 8, new DateTime(2019, 10, 25, 13, 5, 21, 687, DateTimeKind.Utc).AddTicks(1750), new DateTime(2016, 8, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 1, 5 }
                });

            migrationBuilder.InsertData(
                table: "PropertyImages",
                columns: new[] { "Id", "PropertyId", "Url" },
                values: new object[,]
                {
                    { 7, 4, "https://images.finncdn.no/dynamic/1280w/2019/10/vertical-2/25/1/161/351/381_1844547171.jpg" },
                    { 8, 4, "https://images.finncdn.no/dynamic/1280w/2019/10/vertical-2/25/1/161/351/381_983293329.jpg" },
                    { 9, 5, "https://images.finncdn.no/dynamic/1600w/2019/8/vertical-2/15/0/146/907/100_179405212.jpg" },
                    { 10, 5, "https://images.finncdn.no/dynamic/1280w/2019/8/vertical-2/15/0/146/907/100_1830835313.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Renovations",
                columns: new[] { "Id", "DateFrom", "DateTo", "Description", "PropertyId" },
                values: new object[,]
                {
                    { 6, new DateTime(2019, 5, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 7, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "Built two new bedrooms", 4 },
                    { 7, new DateTime(2019, 5, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 7, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "Built two new bedrooms", 5 },
                    { 8, new DateTime(2019, 2, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 7, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "Extended north side of the house, in order to build a new livingroom, also added an extra bathroom", 5 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "cb097e14-8133-444b-b493-fa0dff0417d2");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "f352b769-cfc1-4224-b9e1-1bb0b2467d8b");

            migrationBuilder.DeleteData(
                table: "OwnershipLogs",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "OwnershipLogs",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "OwnershipLogs",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "OwnershipLogs",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PropertyImages",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PropertyImages",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PropertyImages",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PropertyImages",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Renovations",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Renovations",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Renovations",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Discriminator", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "AccountTypeId", "Birthday", "CreatedAt", "FirstName", "LastName" },
                values: new object[,]
                {
                    { "21a1ff14-8a62-4eb6-b3a9-5edb632e06e5", 0, null, "Account", "buyer@hotmail.com", true, true, null, "BUYER@HOTMAIL.COM", "BUYER@HOTMAIL.COM", "AQAAAAEAACcQAAAAECbumLhYF0rV3eEJfr8zkrrEN+u7oJnNPFpwJdFJTaVjCRdgNpaDrFTaFZcCT+cLOw==", "123456789", false, "", false, "buyer@hotmail.com", 2, new DateTime(2004, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 10, 21, 18, 20, 8, 903, DateTimeKind.Utc).AddTicks(5532), "Mighty", "Guy" },
                    { "a83cb535-21d1-46f3-af84-70a3b8405cb3", 0, null, "Account", "smith@hotmail.com", true, true, null, "SMITH@HOTMAIL.COM", "SMITH@HOTMAIL.COM", "AQAAAAEAACcQAAAAEJ6fCgl+1jur7A56X5ESQaf/9ZCf/SBR/SFWzmgz7/QMNdc/vV8pl1/lXr5Hn6mK2A==", "987654321", false, "", false, "smith@hotmail.com", 1, new DateTime(1999, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 10, 21, 18, 20, 8, 903, DateTimeKind.Utc).AddTicks(9103), "Mr", "Smith" }
                });

            migrationBuilder.UpdateData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 21, 18, 20, 8, 886, DateTimeKind.Utc).AddTicks(5898));

            migrationBuilder.UpdateData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 21, 18, 20, 8, 886, DateTimeKind.Utc).AddTicks(8405));

            migrationBuilder.UpdateData(
                table: "OwnershipLogs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 21, 18, 20, 8, 888, DateTimeKind.Utc).AddTicks(3504));

            migrationBuilder.UpdateData(
                table: "OwnershipLogs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 21, 18, 20, 8, 888, DateTimeKind.Utc).AddTicks(5038));

            migrationBuilder.UpdateData(
                table: "OwnershipLogs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 21, 18, 20, 8, 888, DateTimeKind.Utc).AddTicks(6706));

            migrationBuilder.UpdateData(
                table: "OwnershipLogs",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 21, 18, 20, 8, 888, DateTimeKind.Utc).AddTicks(6773));

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 21, 18, 20, 8, 887, DateTimeKind.Utc).AddTicks(3973));

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 21, 18, 20, 8, 887, DateTimeKind.Utc).AddTicks(5958));

            migrationBuilder.UpdateData(
                table: "Properties",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2019, 10, 21, 18, 20, 8, 887, DateTimeKind.Utc).AddTicks(6022));
        }
    }
}
