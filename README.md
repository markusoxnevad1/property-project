# Property Project

This project is a software solution that gives access to a catalogue of properties available to real-estate agents and prospetive buyers.
In cooperation with Jon Erik Selland, Sondre Waage Tofte and Markus Øxnevad.

# API Doc
**Login**
----
  Receive login credentials and return json web token

* **URL**

  /authentication

* **Method:**
  
  `POST`

* **Data Params**

```json
    {
        Email: [string],
        Password: [string]
    }
```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ jwt-string }`
 
* **Error Response:**
  
  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ Invalid username and/or password }`

  OR

  * **Code:** 429 TOO MANY REQUESTS <br />
    **Content:** `{ Too many requests, please wait N minutes }`


**Guest**
----
  Return guest token

* **URL**

  /authentication

* **Method:**
  
  `GET`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ jwt-string }`


**Get all properties**
----
  Return basic information about every property

* **URL**

  /api/property

* **Method:**
  
  `GET`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ [PropertyObject... PropertyObjectN] }`
    **PropertyObject:**
    ```json
        {
            id: [int],
            title: [string],
            shortAddress: [string],
            city: [string],
            propertyImages: [ImageObject... ImageObjectN]
        }
    ```
    **ImageObject:**
    ```json
        {
            id: [int],
            url: [string]
        }
    ```
 
* **Error Response:**

  * **Code:** 401 UNAUTHORIZED <br />


**Get one property**
----
  Return details about a single property. The amount of detail information depend on the current user's role.

* **URL**

  /api/property/id

* **Method:**
  
  `GET`
  
*  **URL Params**

   **Required:**
 
   `id=[integer]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ PropertyObject }`
    **PropertyObject_Guest:**
    ```json
        {
            id: [int],
            title: [string],
            shortAddress: [string],
            longAddress: [string],
            propertyType: [string],
            propertyImages: [ImageObject... ImageObjectN]
        }
    ```
    **PropertyObject_Buyer:**
    ```json
        {
            ...PropertyObject_Guest,
            city: [string],
            zip: [int],
            builtAt: [DateTime],
            lastRenovation: [DateTime]
        }
    ```
    **PropertyObject_Agent:**
    ```json
        {
            ...PropertyObject_Buyer,
            currentOwnerName: [string],
            currentOwnerTlf: [string],
            currentOwnerEmail: [string],
            currentPropertyValue: [int],
            renovations: [Renovation... RenovationN],
            ownershipLogs: [OwnershipLog... OwnershipLogN]
        }
    ```
    **Renovation:**
    ```json
        {
            id: [int],
            description: [string],
            dateFrom: [DateTime],
            dateTo: [DateTime]
        }
    ```
    **OwnershipLog:**
    ```json
        {
            id: [int],
            dateAcquired: [DateTime],
            dateSold: [DateTime],
            firstName: [string],
            lastName: [string]
        }
    ```
    **ImageObject:**
    ```json
        {
            id: [int],
            url: [string]
        }
    ```
 
* **Error Response:**

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "Log in" }`

  OR

  * **Code:** 422 UNPROCESSABLE ENTRY <br />


**Get one user**
----
  Return a single user object

* **URL**

  /api/account/id

* **Method:**
  
  `GET`
  
*  **URL Params**

   **Required:**
 
   `id=[integer]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```json
        {
            email: [string],
            firstName: [string],
            lastName: [string],
            phone: [int]
        }
    ```
 
* **Error Response:**

  * **Code:** 401 UNAUTHORIZED <br />

  OR

  * **Code:** 403 FORBIDDEN <br />


**Update one user**
----
  Receive data and applies them to the user

* **URL**

  /api/account/id

* **Method:**
  
  `PUT`
  
*  **URL Params**
  
   **Required:**
 
   `id=[integer]`

* **Data Params**

  ```json
        {
            Email: [string],
            Phone:[string || int],
            FirstName: [string],
            LastName: [string]
        }
    ```

* **Success Response:**
  
  * **Code:** 204 <br />
 
* **Error Response:**

  * **Code:** 401 UNAUTHORIZED <br />

  OR

  * **Code:** 400 BAD REQUEST <br />

  OR

  * **Code:** 403 FORBIDDEN <br />



**Update password for user**
----
  Update the password of the logged in user

* **URL**

  /api/account/changepassword/id

* **Method:**
  
  `PUT`
  
*  **URL Params**

   **Required:**
 
   `id=[integer]`

* **Data Params**

  ```json
        {
            OldPassword = [string],
            NewPassword = [string]
        }
    ```

* **Success Response:**
  
  * **Code:** 200 <br />
    **Content:** `{ success: true, errors: [] }`
 
* **Error Response:**

  * **Code:** 401 UNAUTHORIZED <br />

  OR

  * **Code:** 400 BAD REQUEST <br />
    **Content:**
    ```json
        { 
            success: false, 
            errors: [ { description: [string], code: [string] } ... ]
        }
    ```


**Verify token**
----
  Verifies the token in header

* **URL**

  /authentication/verify

* **Method:**
  
  `GET`

* **Success Response:**

  * **Code:** 200 <br />
 
* **Error Response:**

  * **Code:** 401 UNAUTHORIZED <br />